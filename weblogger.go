package weblogger

import (
	"net/http"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/mstarongitlab/goutils/sliceutils"
)

// Copied from https://arunvelsriram.medium.com/simple-golang-http-logging-middleware-315656ff8722
// and https://drstearns.github.io/tutorials/gomiddleware/
// and https://blog.kowalczyk.info/article/e00e89c3841e4f8c8c769a78b8a90b47/logging-http-requests-in-go.html

type (
	Logger struct {
		handler http.Handler
		config  *Config
	}

	// struct for holding response details
	responseData struct {
		status int
		size   int
	}

	// our http.ResponseWriter implementation
	loggingResponseWriter struct {
		http.ResponseWriter // compose original http.ResponseWriter
		responseData        *responseData
	}
)

var defaultConfig Config = Config{
	DefaultLogLevel:    LOG_LEVEL_DEBUG,
	FailedRequestLevel: LOG_LEVEL_ERROR,
}

func (r *loggingResponseWriter) Write(b []byte) (int, error) {
	size, err := r.ResponseWriter.Write(b) // write response using original http.ResponseWriter
	r.responseData.size += size            // capture size
	return size, err
}

func (r *loggingResponseWriter) WriteHeader(statusCode int) {
	r.ResponseWriter.WriteHeader(statusCode) // write status code using original http.ResponseWriter
	r.responseData.status = statusCode       // capture status code
}

func LoggingMiddleware(
	handler http.Handler,
	config *Config,
) *Logger {
	if config == nil {
		config = &defaultConfig
	}
	return &Logger{
		handler: handler,
		config:  config,
	}
}

func (l *Logger) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if sliceutils.Contains(l.config.ExcludeRoutes, r.URL.Path) {
		l.handler.ServeHTTP(w, r)
		return
	}
	start := time.Now()

	addr := requestGetRemoteAddress(r)
	logger := log.With().
		Str("method", r.Method).
		Str("caller", addr).
		Str("path", r.URL.Path).
		Logger()

	logWithLevel(l.config.DefaultLogLevel, &logger, "Request incoming")

	responseData := &responseData{
		status: 0,
		size:   0,
	}
	lrw := loggingResponseWriter{
		ResponseWriter: w,
		responseData:   responseData,
	}

	l.handler.ServeHTTP(&lrw, r)

	logger = logger.With().
		Dur("request-duration", time.Since(start)).
		Int("status", responseData.status).
		Logger()
	if responseData.status > 300 {
		logWithLevel(l.config.FailedRequestLevel, &logger, "Request failed")
		return
	}
	logWithLevel(l.config.DefaultLogLevel, &logger, "Request complete")
}

// Request.RemoteAddress contains port, which we want to remove i.e.:
// "[::1]:58292" => "[::1]"
func ipAddrFromRemoteAddr(s string) string {
	idx := strings.LastIndex(s, ":")
	if idx == -1 {
		return s
	}
	return s[:idx]
}

// requestGetRemoteAddress returns ip address of the client making the request,
// taking into account http proxies
func requestGetRemoteAddress(r *http.Request) string {
	hdr := r.Header
	hdrRealIP := hdr.Get("X-Real-Ip")
	hdrForwardedFor := hdr.Get("X-Forwarded-For")
	if hdrRealIP == "" && hdrForwardedFor == "" {
		return ipAddrFromRemoteAddr(r.RemoteAddr)
	}
	if hdrForwardedFor != "" {
		// X-Forwarded-For is potentially a list of addresses separated with ","
		parts := strings.Split(hdrForwardedFor, ",")
		for i, p := range parts {
			parts[i] = strings.TrimSpace(p)
		}
		// TODO: should return first non-local address
		return parts[0]
	}
	return hdrRealIP
}

func logWithLevel(level LoggingLevel, entry *zerolog.Logger, msg string) {
	switch level {
	case LOG_LEVEL_DEBUG:
		entry.Debug().Msg(msg)
	case LOG_LEVEL_INFO:
		entry.Info().Msg(msg)
	case LOG_LEVEL_WARN:
		entry.Warn().Msg(msg)
	case LOG_LEVEL_ERROR:
		entry.Error().Msg(msg)
	default:
		entry.Debug().Msg(msg)
	}
}
