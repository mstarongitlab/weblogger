package weblogger

type LoggingLevel int

const (
	LOG_LEVEL_DEBUG = LoggingLevel(iota)
	LOG_LEVEL_INFO
	LOG_LEVEL_WARN
	LOG_LEVEL_ERROR
)

type Config struct {
	// The level at which requests are logged (corresponds to logrus levels)
	DefaultLogLevel LoggingLevel
	// The level at which failed requests are logged
	FailedRequestLevel LoggingLevel
	// Routes to not log access to
	ExcludeRoutes []string
}
