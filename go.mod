module gitlab.com/mstarongitlab/weblogger

go 1.21.5

require (
	github.com/rs/zerolog v1.33.0
	gitlab.com/mstarongitlab/goutils v0.0.0-20240221131250-70f6d1947636
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
