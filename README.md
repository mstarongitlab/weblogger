# Basic weblogger for Go's webserver

Logs method, caller, path, timestamp, and execution time

Example usage:

```go
default_mux := http.DefaultServeMux
 logger := LoggingMiddleware(default_mux, nil)

 if err := http.ListenAndServe(":"+port, logger); err != nil {
    panic(err)
 }
```
